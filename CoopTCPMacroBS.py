import select
import socket
import struct
import pickle
import protocol
from collections import Counter
import numpy as np

sbs_num = 2
class TCPServer:
    def __init__(self, ipaddress, port, listen_num):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind((ipaddress, port))

        self.server_socket.listen(listen_num)
        self.inputs = [self.server_socket]
        self.outputs = []
        self.rec_list=[]
        self.dict_mess = {}
        self.count_dict={}
        self.name_list=[]

        self.sbs_num = sbs_num
        self.TopN = 50
        self.TopSBS = 100
        self.cache_num = self.TopN+self.TopSBS
        self.rec_arr = np.zeros((self.sbs_num, self.cache_num))

    def accept_client(self):
        connection, client_address = self.server_socket.accept()
        print("Connection from {}".format(client_address))
        self.inputs.append(connection)
        self.dict_mess[connection] = {"flag": '', "mess_data": []}
        self.count_dict[connection] = 0

    def _recvall(self, sock, count):
        buf = b''
        while count:
            newbuf = sock.recv(count)
            if not newbuf: return None
            buf += newbuf
            count -= len(newbuf)
        return buf

    def tcp_read(self, sock):
        lengthbuf = self._recvall(sock, 4)
        if lengthbuf == None:
            return lengthbuf

        length, = struct.unpack('!I', lengthbuf)
        data = self._recvall(sock, length)
        message = pickle.loads(data)
        return message

    def tcp_send(self, sock, message):
        data = pickle.dumps(message)
        length = len(data)
        sock.sendall(struct.pack('!I', length))
        sock.sendall(data)
        print('send', message, 'to', sock.getpeername())
        if sock in self.outputs:
            self.outputs.remove(sock)

    def mbs_send_handler(self,socket, data):
        self.dict_mess[socket]["flag"]= protocol.MBS_SEND_DATA
        print('LENGTH:',len(data))
        self.sbs_neighbor = np.array(data[:, 0:self.TopN]).tolist()
        temp = []
        for i in self.sbs_neighbor:
            for j in i:
                temp.append(j)
        self.sbs_neighbor = temp
        self.mbs_list=[]
        temp_mbs = []
        temp_mbs_list = np.array(data[:, self.TopN:]).tolist()
        for item in temp_mbs_list:
            for ii in item:
                temp_mbs.append(ii)
        Ranking = Counter(temp_mbs)
        Ranking_top = Ranking.most_common(self.TopSBS)
        for i in Ranking_top:
            self.mbs_list.append(i[0])

        print('length of SBS Neighbor:', len(self.sbs_neighbor))
        print('length of MBS:', len(self.mbs_list))
        final_send_list = self.sbs_neighbor+self.mbs_list
        self.dict_mess[socket]["mess_data"] = final_send_list


    def run(self, timeout = 30):
        while self.inputs:
            print("Waiting events!")
            readable, writable, exceptional = select.select(self.inputs, self.outputs, self.inputs, timeout)

            if not (readable or writable or exceptional):
                print("Time out ! ")
                break

            for s in readable:
                if s is self.server_socket:
                    self.accept_client()
                else:
                    data = self.tcp_read(s)
                    if data:
                        if data["flag"] == protocol.SBS_SEND_MBS:
                            name = s.getpeername()
                            if name not in self.name_list:
                                row=len(self.name_list)
                                print('length:',len(data["mess_data"][0:self.cache_num]))
                                self.rec_arr[row,:] = data["mess_data"][0:self.cache_num]
                                self.name_list.append(name)
                            if len(self.name_list) == self.sbs_num:
                                print('Shape of rec_arr', self.rec_arr.shape)
                                self.mbs_send_handler(s,self.rec_arr)
                            print(" received {} from {}".format(data, s.getpeername()))
                            self.outputs.append(s)
                    else:
                        # Interpret empty result as closed connection
                        print("   closing")
                        if s in self.outputs:
                            self.outputs.remove(s)
                        self.inputs.remove(s)
                        s.close()
            for s in writable:
                self.tcp_send(s, self.dict_mess[s])

            for s in exceptional:
                print (" exception condition on ", s.getpeername())
                # stop listening for input on the connection
                self.inputs.remove(s)
                if s in self.outputs:
                    self.outputs.remove(s)
                s.close()

tcp_server = TCPServer("127.0.0.1", 10054, 10)
tcp_server.run(timeout=10000)