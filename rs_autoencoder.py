import socket
import pickle
import io
import time
import struct
import protocol
import select
import config
import argparse
import numpy as np
import os
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session


client_index = 2
top_num = 50
mbs_num= 100
send_dict = {"flag": 1, "message": []}
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
config_gpu = tf.ConfigProto()
config_gpu.gpu_options.per_process_gpu_memory_fraction = 0.2
set_session(tf.Session(config=config_gpu))

class Data:
    def __init__(self):

        np.random.seed(1337)  # for reproducibility
        self.TopN = top_num
        self.mbsN = mbs_num
        self.select_num = 50
        self.rec_list = []
        self.total_rec_list = []
        self.initNetwork()
        self.sbs_rec_list = []
        self.recall_list = []
        self.initNetwork()
        self.loadData(1)

    def loadData(self, client_index):
        self.RatingMatrix = np.loadtxt('data/ml_dataset/train_user' + str(client_index) + '.txt')
        # self.RatingMatrix1 = np.loadtxt('data/ml_dataset/train_user1.txt')
        # self.RatingMatrix2 = np.loadtxt('data/ml_dataset/train_user2.txt')
        # self.RatingMatrix = np.concatenate([self.RatingMatrix1,self.RatingMatrix2])
        # print(self.RatingMatrix.shape)
        self.TestMatrix = np.loadtxt('data/ml_dataset/test_user' + str(client_index) + '.txt')
        # self.TestMatrix1 = np.loadtxt('data/ml_dataset/test_user1.txt')
        # self.TestMatrix2 = np.loadtxt('data/ml_dataset/test_user2.txt')
        # self.TestMatrix = np.concatenate([self.TestMatrix1,self.TestMatrix2])
        self.RatingMatrix[self.RatingMatrix > 0] = 1
        self.TestMatrix[self.TestMatrix > 0] = 1
        self.RatingMatrix = self.RatingMatrix.astype(np.int64)
        self.TestMatrix = self.TestMatrix.astype(np.int64)
        self.RatingMask = config.get_mask(self.RatingMatrix)
        self.TestMask = np.ones(self.TestMatrix.shape)
        requestList = []
        for i in range(self.TestMatrix.shape[0]):
            wuid = np.where(self.TestMatrix[i, :] > 0)
            wuid = np.array(wuid)
            for i in wuid[0]:
                requestList.append(i)
        self.TestList = requestList
        self.RatingMatrixItem = self.RatingMatrix.copy()
        self.train()
        self.predict()
        self.test(self.rec_list)
        self.getNetwork()

    def initNetwork(self):
        # deep autoencoders based on Keras
        self.autoencoder = config.get_autoencoder(config.item_network_input_dim, config.item_network_hidden_layer, "item")


    def setNetwork(self, weight):
        self.autoencoder.set_weights(weight)

    def getNetwork(self):

        weight = self.autoencoder.get_weights()

        return weight

    def train(self):

        x = self.RatingMatrixItem
        print('Append Matrix: ', x.shape)

        # training
        self.autoencoder.fit(x=[x, self.RatingMask], y=x, epochs=4, batch_size= 64, shuffle=True)

    def predict(self):
        t1 = time.time()
        pred = self.autoencoder.predict(x=[self.RatingMatrixItem, self.TestMask])
        pred = pred * (self.RatingMatrixItem == 0)  # remove watched items from predictions
        pred = np.argsort(pred)
        rec = pred[:, -self.mbsN:]
        recommended = np.zeros(self.RatingMatrixItem.shape)
        for i in range(rec.shape[0]):
            recommended[i][rec[i]] = 1
        rec_sum = recommended.sum(axis=0)
        rec_sum_sort = np.argsort(rec_sum)
        cache = rec_sum_sort[-self.select_num:]
        self.rec_list = cache.tolist()
        print ('REC LIST:',len(self.rec_list))
        t2 = time.time()
        t = t2-t1
        print('predeict time:', t)

    def test(self,serverList):

        print('TO BE TEST LIST:', serverList)
        count = self.TestMatrix.shape[0]
        rcl = 0

        for i in range(0,self.TestMatrix.shape[0]):  # self.rec_top_N(2)
            rating = self.TestMatrix[i, :]
            vindex = np.where(rating > 0)
            vindex = np.array(vindex)
            if vindex.shape[1] == 0:
                print("Ignored all zero ...")
                count -= 1

            rtrue = 0
            for rlist in serverList:
                for ulist in vindex[0, :]:
                    if rlist == ulist:
                        rtrue += 1
            if vindex.shape[1] > 0:
                recall = rtrue / vindex.shape[1]
                rcl = rcl + recall

                #print("For user ", i, "the cache efficiency@", self.TopN, " is:", rtrue, "/", vindex.shape[1], "=", recall)

        print("The overall cache efficiency@", self.TopN, " is: ", rcl / count)
        if (rcl/ count > 1):
            np.savetxt("data/wrong.txt", serverList)
        self.recall_list.append(rcl / count)
        print("The recall list:", self.recall_list)
        print("*****************Total list to cal******************")
        new_rtrue = 0
        self.TestList = np.array(self.TestList).tolist()
        for tlist in self.TestList:
            for slist in serverList:
                if int(slist) == int(tlist):
                    new_rtrue += 1

        total_list_recall = new_rtrue / len(self.TestList)

        print("The total test list @cache efficiency@ is: ", total_list_recall)
        self.total_rec_list.append(total_list_recall)
        print("The total test list : ", self.total_rec_list)

def main():
    data = Data()

if __name__ == '__main__':
    main()