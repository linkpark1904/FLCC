import numpy

def success_rate(pred, true):
    cnt = 0
    for i in range(pred.shape[0]):
        t = numpy.where(true[i] == 1) # true set
        sum = true[i].sum()
        ary = numpy.intersect1d(pred[i], t)
        cnt += (ary.size / sum)
    return cnt * 100 / pred.shape[0]

def cache_efficiency(pred, true):
    recommended = numpy.zeros(true.shape)
    size = pred.shape[1]
    for i in range(pred.shape[0]):
        recommended[i][pred[i]] = 1
    rec_sum = recommended.sum(axis=0)
    rec_sum_sort = numpy.argsort(rec_sum)
    cache = rec_sum_sort[-size:]
    print(cache)
    print(rec_sum[cache])
    cnt = 0
    for i in range(true.shape[0]):
        t = numpy.where(true[i] == 1)  # true set
        sum = true[i].sum()
        ary = numpy.intersect1d(cache, t)
        cnt += (ary.size / sum)
    return cnt * 100 / pred.shape[0]