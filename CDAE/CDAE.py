from keras.layers import Input, Dense, Embedding, Flatten, Dropout, merge, Activation
from keras.models import Model
from keras.regularizers import l2


def create(I, U, K, hidden_activation, output_activation, q=0.2, l=0.01):

    x_item = Input((I,), name='x_item')
    h_item = Dropout(q)(x_item)
    h_item = Dense(K, W_regularizer=l2(l), b_regularizer=l2(l))(h_item)
       # dtype should be int to connect to Embedding layer
    x_user = Input((1,), dtype='int32', name='x_user')
    x_mask = Input((I,), name='x_mask')
    h_user = Embedding(input_dim=U, output_dim=K, input_length=1, W_regularizer=l2(l))(x_user)
    h_user = Flatten()(h_user)

    h = merge([h_item, h_user], mode='sum')
    if hidden_activation:
        h = Activation(hidden_activation)(h)
    y = Dense(I, activation=output_activation)(h)
    y_masked = merge([y, x_mask], mode="mul")
    return Model(input=[x_item, x_user, x_mask], output=y_masked)