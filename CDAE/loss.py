import keras.backend as K
def loss(y_true, y_pred):
    return K.sum(K.square(y_pred - y_true), axis=-1)