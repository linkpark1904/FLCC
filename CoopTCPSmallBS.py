import select
import socket
import struct
import pickle
from collections import Counter
import numpy as np
import argparse
import config
import protocol

client_num = 2
top_num = 50
mbs_num = 150

class Data:
    def __init__(self):

        np.random.seed(1337)  # for reproducibility
        self.initNetwork()
        self.mbs_rec_list = []
        self.topN = top_num
        self.mbsN = mbs_num
        self.totalN = self.topN + self.mbsN


    def initNetwork(self):
        # deep autoencoders based on Keras
        # this is our input placeholder
        self.autoencoder = config.get_autoencoder(config.item_network_input_dim, config.item_network_hidden_layer, "item")

    def setNetwork(self, weight):
        self.autoencoder.set_weights(weight)

    def getNetwork(self):
        weight = self.autoencoder.get_weights()
        return weight

    def countAcc(self,send_from_client):
        rec_list=[]
        temp=[]
        for li in send_from_client:
            for ji in li:
                temp.append(ji)
        print('Before counter', len(temp))
        Ranking = Counter(temp)
        Ranking_top = Ranking.most_common(self.totalN)
        for i in Ranking_top:
            rec_list.append(i[0])

        self.rec_list=rec_list
        self.sbs_rec_list = rec_list[:self.topN]
        self.mbs_rec_list = rec_list[self.topN:self.totalN]
        print('sbs rec list:', len(self.sbs_rec_list))
        print('mbs rec list:', len(self.mbs_rec_list))

class TCPServer:
    def __init__(self, ipaddress, port, listen_num):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind((ipaddress, port))
        self.server_socket.listen(listen_num)
        self.inputs = [self.server_socket]
        self.outputs = []
        self.send_dict = {}
        self.count_dict = {}
        self.data_class = Data()
        self.client_name_list = []
        self.rec_client_list=[]
        self.name_list=[]
        self.arr_agg = None
        self.client_sockets= []

        address = ("127.0.0.1", 10054)
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect(address)
        self.inputs.append(self.client_socket)

        self.send_dict[self.client_socket] = {"flag":'',"mess_data":[]}
        self.round_finish = False
        self.client_num = client_num
        self.count_client = 0
        self.round = 2
        self.connected_client_num = 0
        self.sbs_rec_list = []
        self.temp=[]

    def accept_client(self):
        connection, client_address = self.server_socket.accept()
        print("Connection from {}".format(client_address))
        self.connected_client_num += 1
        self.inputs.append(connection)
        self.client_sockets.append(connection)
        #self.send_dict[connection] = {"flag":protocol.SEND_INIT,"mess_data":self.data_class.getNetwork()}
        self.send_dict[connection] = {"flag":'',"mess_data":''}
        self.init_handler(connection)
        self.count_dict[connection] = 0
        #print(self.inputs)

        print("count number ", self.connected_client_num)
        if self.connected_client_num == client_num:
            print("begin to send data!")
            for socket in self.client_sockets:
                self.outputs.append(socket)

    def _recvall(self, sock, count):
        buf = b''
        while count:
            newbuf = sock.recv(count)
            if not newbuf: return None
            buf += newbuf
            count -= len(newbuf)
        return buf

    def tcp_read(self, sock):
        lengthbuf = self._recvall(sock, 4)
        if lengthbuf == None:
            return lengthbuf

        length, = struct.unpack('!I', lengthbuf)
        data = self._recvall(sock, length)
        message = pickle.loads(data)
        return message

    def tcp_send(self, sock, message):
        data = pickle.dumps(message)
        length = len(data)
        sock.sendall(struct.pack('!I', length))
        sock.sendall(data)
        #print('send', message, 'to', sock.getpeername())
        if sock in self.outputs:
            self.outputs.remove(sock)

    def init_handler(self,socket):
        self.send_dict[socket]["flag"] = protocol.SEND_INIT
        self.send_dict[socket]["mess_data"] = self.data_class.getNetwork()
        print("SEND INIT DATA TO CLIENT")

    def para_agg_handler(self,socket,data):
        self.send_dict[socket]["flag"] = protocol.SEND_PARA
        if self.arr_agg == None:
            self.arr_agg = data
        else:
            for i, arr in enumerate(data):
                self.arr_agg[i] += arr

    def cal_para_handler(self):
        for i, arr in enumerate(self.arr_agg):
            self.arr_agg[i] = arr/self.client_num

    def para_handler(self,socket, data):
        self.send_dict[socket]["flag"] = protocol.SEND_PARA
        self.send_dict[socket]["mess_data"] = self.arr_agg
        print("SEND PARA TO CLIENT", self.arr_agg)

    def roundEnd_handler(self,socket,data):
        self.send_dict[socket]["flag"] = protocol.SEND_REC
        print("SEND Final round PARA TO CLIENT")
        self.send_dict[socket]["mess_data"] = self.arr_agg

    def send_sbs_to_mbs_handler(self,socket,data):
        self.send_dict[socket]["flag"] = protocol.SBS_SEND_MBS
        self.send_dict[socket]["mess_data"] = self.data_class.rec_list

    def send_final_list_handler(self, socket,data):
        self.send_dict[socket]["flag"]=protocol.SBS_TO_CLIENT_SEND_FINAL
        print('self.sbs_rec_list',len(self.sbs_rec_list))
        print('data from mbs', len(data))
        self.final_list = self.sbs_rec_list + data
        self.send_dict[socket]["mess_data"] = self.final_list
    def end_handler(self,socket,data):
        print(" Round Finished.....   closing")
        print("   closing")
        if socket in self.outputs:
            self.outputs.remove(socket)
        self.inputs.remove(socket)
        socket.close()

    def run(self, timeout = 30):
        while self.inputs:
            #print("Waiting events!")
            readable, writable, exceptional = select.select(self.inputs, self.outputs, self.inputs, timeout)

            if not (readable or writable or exceptional):
                print("Time out ! ")
                break

            for s in readable:
                if s is self.client_socket:
                    data = self.tcp_read(s)
                    if data:
                        print('RECV DATA:', data, 'from', s.getpeername())
                        if data["flag"] == protocol.MBS_SEND_DATA:
                            #print("send dict length {}".format(len(self.send_dict.items())))
                            for socket, msg in self.send_dict.items():
                                if socket is not self.client_socket:
                                    self.send_final_list_handler(socket, data['mess_data'])
                                    self.outputs.append(socket)

                elif s is self.server_socket:
                    self.accept_client()
                    if self.count_client == self.client_num:
                        self.count_client = 0

                else:
                    data = self.tcp_read(s)

                    if data:
                        print('RECV DATA:', data,'from',s.getpeername())
                        if data["flag"] == protocol.SEND_PARA:
                            self.count_dict[s] += 1
                            # print("self.count_dict[s]", self.count_dict[s], 'Round', self.round)
                            print('############# Client:', s.getpeername(), '############ Round:', self.count_dict[s])

                            # count client number, then agg para
                            client_name = s

                            if client_name not in self.client_name_list:
                                self.client_name_list.append(client_name)
                                self.para_agg_handler(s, data["mess_data"])
                                print('Client list:', self.client_name_list, 'length:', len(self.client_name_list))

                            if len(self.client_name_list) == self.client_num:
                                print('A round finished')
                                self.cal_para_handler()
                                for soc in self.client_name_list:
                                    if soc not in self.outputs:
                                        self.outputs.append(soc)
                                    self.roundEnd_handler(soc, data)
                                    self.client_name_list = []

                        if data["flag"] == protocol.CLIENT_LIST_TO_SBS:
                            print("sum value is {} !!!!!!".format(sum(self.count_dict.values())))
                            soc_name=s.getpeername()
                            if soc_name not in self.name_list:
                                self.name_list.append(soc_name)
                                self.rec_client_list.append(data["mess_data"])
                            if len(self.name_list) == self.client_num:
                                self.data_class.countAcc(self.rec_client_list)
                                self.rec_client_list=[]
                                self.send_sbs_to_mbs_handler(self.client_socket, data)
                                self.outputs.append(self.client_socket)

                            if sum(self.count_dict.values()) >= self.client_num * self.round and self.round_finish == False:
                                self.round_finish = True
                                self.send_sbs_to_mbs_handler(self.client_socket, data)
                                self.outputs.append(self.client_socket)

                        if data["flag"] == protocol.Round_Finish:
                            if s not in self.temp:
                                self.temp.append(s)
                            if self.round == len(self.temp):
                                for soc in self.temp:
                                    self.end_handler(soc,data)

                    else:
                        # Interpret empty result as closed connection
                        print("   closing")
                        if s in self.outputs:
                            self.outputs.remove(s)
                        self.inputs.remove(s)
                        s.close()

            for s in writable:
                self.tcp_send(s, self.send_dict[s])

            for s in exceptional:
                print (" exception condition on ", s.getpeername())
                # stop listening for input on the connection
                self.inputs.remove(s)
                if s in self.outputs:
                    self.outputs.remove(s)
                s.close()