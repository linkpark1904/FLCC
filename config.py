from keras.datasets import mnist
from keras.models import Model #泛型模型
from keras.layers import Dense, Input
from CDAE import aCSDAE
from keras import backend as K
from CDAE import mask, loss
from keras import optimizers


#def loss(y_true, y_pred):
#    y_mask = y_true / (y_true + 1e-9)
#    return K.sum(K.square(y_true - (y_mask * y_pred)), axis=-1)


def get_autoencoder(input_dim, hidden_layer: list, name):
    model = aCSDAE.create(I=input_dim, K=hidden_layer, hidden_activation="relu",
                  output_activation="sigmoid", q=drop_out_rate, l=regularization_term)
    #adam = optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss="mse", optimizer='adam')
    return model

def get_mask(data):
    return mask.get_mask(data, mask_method)

item_network_input_dim = 3883
item_network_hidden_layer = [64]
drop_out_rate = 0.2
regularization_term = 1e-4
mask_method = "user"